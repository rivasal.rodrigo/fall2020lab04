package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shape [] shape = new Shape[5];
		shape[0] = new Rectangle(5,2);
		shape[1] = new Rectangle(4,3);
		shape[2] = new Circle(3);
		shape[3] = new Circle(1);
		shape[4] = new Square(3);
		
		
		for(Shape s: shape) {
			System.out.println("Area: "+s.getArea() + " Perimeter: "+s.getPerimeter());
		}
		
	}

}
