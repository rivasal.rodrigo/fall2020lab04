package inheritance;

public class BookStore {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Book [] book = new Book[5];
		
		book[0] = new Book("Harry Potter: Chamber of secret","J.K Rowling");
		book[1] = new ElectronicBook("The Turn of the Screw", "Henry James", 400);
		book[2] = new Book("The Haunting of Hill House","Shirley Jackson");
		book[3] = new ElectronicBook("Doctor Sleep", "Stephen King", 1024);
		book[4] = new ElectronicBook("It", "Stephen King", 900);
		
		
		for(Book i: book) {
			System.out.println(i.toString());
		}
	}

}
